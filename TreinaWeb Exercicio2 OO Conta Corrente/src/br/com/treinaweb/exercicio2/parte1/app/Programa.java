package br.com.treinaweb.exercicio2.parte1.app;

import br.com.treinaweb.exercicio2.parte1.classes.ContaCorrente;
import br.com.treinaweb.exercicio2.parte1.classes.ContaCorrenteFlex;
import br.com.treinaweb.exercicio2.parte1.classes.ContaCorrenteGold;
import br.com.treinaweb.exercicio2.parte1.classes.ContaCorrenteSimples;
import br.com.treinaweb.exercicio2.parte1.classes.Correntista;

public class Programa {

	public static void main(String[] args) {
		
		Correntista correntista = new Correntista();
		
		correntista.setNome("Leandro");
		correntista.setSobrenome("Santos");
		correntista.setIdade(25);
		correntista.setCPF(798276469);


		System.out.println("Nome do Correntista: " + correntista.getNome());
		System.out.println("Sobrenome: " + correntista.getSobrenome());
		System.out.println("Idade: " + correntista.getIdade());
		System.out.println("CPF: " + correntista.getCPF());
		System.out.println("                   ");
		
		correntista.setConta(new ContaCorrenteFlex(10000));		
		ContaCorrente conta = correntista.getConta();
		System.out.println("Conta Corrente Gold");
		System.out.println("Saldo em Conta: " + conta.getSaldo());
		conta.Deposito(100);
		System.out.println("Saldo Pos deposito: " + conta.getSaldo());
		conta.Saque(200);
		System.out.println("Saldo Pos saque: " + conta.getSaldo());
		
	}


}
