package br.com.treinaweb.exercicio2.parte1.classes;

public class Correntista {
	
	private String nome;
	private String sobrenome;
	private int idade;
	private int CPF;
	private ContaCorrente conta;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public int getCPF() {
		return CPF;
	}
	public void setCPF(int cPF) {
		this.CPF = cPF;
	}
	
	public ContaCorrente getConta() {
		return conta;
	}
	public void setConta(ContaCorrente conta) {
		this.conta = conta;		
	}
	
	
	public void saque(double valor) {
		conta.Saque(valor);
	}
	
	public void deposito(double valor) {
		conta.Deposito(valor);
	}
	
	public void saldo() {
		conta.getSaldo();
	}

}
