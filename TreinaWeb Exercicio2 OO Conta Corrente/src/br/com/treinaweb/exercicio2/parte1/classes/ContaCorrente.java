package br.com.treinaweb.exercicio2.parte1.classes;

public abstract class ContaCorrente {
	
	//atributos
	private int agencia;
	private int nrConta;
	protected double saldo;
	
	//get e set
	public int getAgencia() {
		return agencia;
	}
	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}
	public int getNrConta() {
		return nrConta;
	}
	public void setNrConta(int nrConta) {
		this.nrConta = nrConta;
	}
	public double getSaldo() {
		return saldo;
	}
	protected void setSaldo(double saldo) {
		this.saldo = saldo;
	}
		
	//metodos
	public ContaCorrente(double saldo) {
		this.saldo = saldo;		
	}
	
	public void Saque(double valor) {
		this.saldo = saldo - valor;
	}
	
	public abstract void Deposito(double valor);
	

}
