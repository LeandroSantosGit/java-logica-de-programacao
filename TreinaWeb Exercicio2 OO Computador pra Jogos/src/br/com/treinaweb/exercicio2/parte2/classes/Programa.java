package br.com.treinaweb.exercicio2.parte2.classes;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Programa {

    public static void main(String[] args) throws ParseException {

        //Cria um novo computador
        Computador computador = new Computador();

        //Define os dados do computador
        computador.setNome("ASUS");
        computador.setPreco(2500.00);

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        Date data = formato.parse("15/05/2016");

        computador.setDataFabricacao(data);       
        
        System.out.println("Computador: " + computador.getNome());
        System.out.println("Pre�o Computador:" + computador.getPreco());
        System.out.println("Data Fabricacao:" + computador.getDataFabricacao());

        //Chama algumas fun��es do computador
        computador.ligar();
        computador.acessarInternet();
        computador.desligar();
        System.out.println("            ");


        //Define um computador Gamer
        ComputadorGamer computadorGamer = new ComputadorGamer();

        //Define os dados do computador Gamer
        computadorGamer.setNome("Alienware");
        computadorGamer.setPreco(7500.00);

        data = formato.parse("15/09/2017");

        computadorGamer.setDataFabricacao(data);
        computadorGamer.setMemoriaPlacaVideo(16);
        
        System.out.println("Computador: " + computadorGamer.getNome());
        System.out.println("Pre�o Computador:" + computadorGamer.getPreco());
        System.out.println("Data Fabricacao:" + computadorGamer.getDataFabricacao());

        //Chama algumas fun��es do computador Gamer
        computadorGamer.ligar();
        computadorGamer.acessarInternet();
        computadorGamer.jogarNeedForSpeed();
        computadorGamer.desligar();
        System.out.println("            ");


        //Define um computador Parrudo
        ComputadorParrudo computadorParrudo = new ComputadorParrudo();

        //Define os dados do computador Parrudo
        computadorParrudo.setNome("Dell");
        computadorParrudo.setPreco(6200.00);

        data = formato.parse("20/07/2017");

        computadorParrudo.setDataFabricacao(data);
        computadorParrudo.setCorGabinete("Neon");
        
        System.out.println("Computador: " + computadorParrudo.getNome());
        System.out.println("Pre�o Computador:" + computadorParrudo.getPreco());
        System.out.println("Data Fabricacao:" + computadorParrudo.getDataFabricacao());

        //Chama algumas fun��es do computador Parrudo
        computadorParrudo.ligar();
        computadorParrudo.acessarInternet();
        computadorParrudo.jogarCounterStrike();
        computadorParrudo.desligar();
        System.out.println("            ");


        //Define um computador Simples
        ComputadorSimples computadorSimples = new ComputadorSimples();

        //Define os dados do computador Parrudo
        computadorSimples.setNome("HP");
        computadorSimples.setPreco(1500.00);

        data = formato.parse("20/02/2017");

        computadorSimples.setDataFabricacao(data);
        
        System.out.println("Computador: " + computadorSimples.getNome());
        System.out.println("Pre�o Computador:" + computadorSimples.getPreco());
        System.out.println("Data Fabricacao:" + computadorSimples.getDataFabricacao());

        //Chama algumas fun��es do computador Parrudo
        computadorSimples.ligar();
        computadorSimples.jogarPaciencia();
        computadorSimples.desligar();
    }
}