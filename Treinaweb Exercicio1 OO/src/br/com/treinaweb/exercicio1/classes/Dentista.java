package br.com.treinaweb.exercicio1.classes;

public class Dentista {
	
	// atributos da classe privados encapsulados
	private String nome;
	private String sobrenome;
	private String especialidade;
	private String registroConcelho;
	private boolean estado;
	
	
	// get(acessar) e set(alterar) acessar de outras classes os atributos
	public String getNome() {
		return nome;
		
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSobrenome() {
		return sobrenome;
		
	}
	
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	
	public String getEspecialidade() {
		return especialidade;
		
	}
	
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
	
	public String getRegistroConcelho() {
		return registroConcelho;
		
	}
	
	public void setRegistroConcelho(String registroConcelho) {
		this.registroConcelho = registroConcelho;
	}
	
	public boolean isEstado() {
		return estado;
	}
	
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	// construtor metodo para inicializar o objeto
	public Dentista (String nome, String sobrenome, String especialidade, String registroConcelho,
			          boolean estado) {
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.especialidade = especialidade;
		this.registroConcelho = registroConcelho;
		this.estado = estado;
				
	}
	
	
	public void atenderPaciente() {
		System.out.println("O " + nome + " esta atendendo paciente!");
	}
	
	public void marcarConsulta() {
		System.out.println("Marcando consulta de paciente!");
	}
	
	public void remarcarConsulta() {
		System.out.println("Remarcando consulta de paciente!");
	}
	
	public void cancelarConsulta() {
		System.out.println("Cancelando consulta de cliente!");
	}
	
	public void viajarCongresso() {
		System.out.println("Indisponivel, esta em um congresso!");
	}

}
