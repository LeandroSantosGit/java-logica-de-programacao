package br.com.treinaweb.exercicio1.app;

import br.com.treinaweb.exercicio1.classes.Dentista;
import br.com.treinaweb.exercicio.classes.Paciente;

public class Programa {

	public static void main(String[] args) {
		
		Dentista dentista = new Dentista("Marcos", "Silva", "Cirurgi�o Dentista", "AM-CS-90",
				                          true);
		
		          dentista.atenderPaciente();
		          dentista.marcarConsulta();
		          dentista.remarcarConsulta();
		          dentista.cancelarConsulta();
		          dentista.viajarCongresso();
		          
		          
		 Paciente paciente = new Paciente(1, "Leandro", "Santos", 25, "Masculino", "92-99531-2659", 
				                          "Rua Peruibe 17- Manaus - AM");
		 
		          paciente.marcarConsulta();
		          paciente.remarcarConsulta();
		          paciente.consultaConsulta();
		          paciente.cancelarConsulta();
		          
	}

}
