package br.com.treinaweb.logica.zoologico.classes;

public class Gato extends Animal {

	public Gato(String nome, int idade, String especie) {
		super(nome, idade, especie);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void emitirBarrulho() {
		if(estaVivo) {
			System.out.println("Miado do Gato");
		}else {
			System.out.println("Animal morreu!");
		}
		
		
	}

}
