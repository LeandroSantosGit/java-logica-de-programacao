package br.com.treinaweb.logica.zoologico.classes;

public abstract class Animal implements Animavel {
	
	// atributos da classe privados encapsulados
	private String nome;
	private String especie;
	protected int idade;
	protected Boolean estaVivo;
	
	
	// get(acessar) e set(alterar) acessar de outras classes os atributos
	public String getNome() {
		return nome;		
	}
	
	public void setNome(String nome) {
		this.nome = nome;		
	}
	
	public String getEspecie() {
		return especie;		
	}
	
	public void setEspecie (String especie) {
		this.especie = especie;
	}
	
	public int getIdade() {
		return idade;
	}
	
	public void setIdade(int idade) {
		this.idade = idade;
	}
	
	public Boolean estaVivo() {
		return estaVivo;
	}
	
	// construtor metodo para inicializar o objeto
	public Animal (String nome, int idade, String especie) {
		this.nome = nome;
	    this.idade = idade;
	    this.especie = especie;
	    this.estaVivo = true;
	}
	
	// Metodos da classe
	
	@Override
	public abstract void emitirBarrulho(); 
	
	@Override
	public Boolean ehAdulto() {
		if(estaVivo) {
			return idade >= 1;
		}else {
			return false;
		}
		
	}
	
	@Override
	public final void morrer() {
		this.estaVivo = false;
	}
	
}
