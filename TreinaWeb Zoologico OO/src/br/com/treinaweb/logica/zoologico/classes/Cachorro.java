package br.com.treinaweb.logica.zoologico.classes;

public class Cachorro extends Animal {

	public Cachorro(String nome, int idade, String especie) {
		super(nome, idade, especie);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void emitirBarrulho() {
		if(estaVivo) {
			System.out.println("Latido do Cachorro");
		}else {
			System.out.println("Animal morreu!");
		}
			
	}
	
	@Override
	public Boolean ehAdulto() {
		if (estaVivo) {
			return idade >= 2;
		}else {
			return false;
		}
	}
}
