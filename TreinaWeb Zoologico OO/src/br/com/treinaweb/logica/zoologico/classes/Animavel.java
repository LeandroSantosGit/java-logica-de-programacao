package br.com.treinaweb.logica.zoologico.classes;

public interface Animavel {
	
	Boolean ehAdulto();
	void emitirBarrulho();
	void morrer();

}
