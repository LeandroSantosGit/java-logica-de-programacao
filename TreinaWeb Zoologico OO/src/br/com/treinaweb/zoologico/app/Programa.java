package br.com.treinaweb.zoologico.app;

import br.com.treinaweb.logica.zoologico.classes.Animal;
import br.com.treinaweb.logica.zoologico.classes.Cachorro;
import br.com.treinaweb.logica.zoologico.classes.Gato;
import br.com.treinaweb.logica.zoologico.classes.Veterinario;
import br.com.treinaweb.logica.zoologico.classes.Zoologico;

public class Programa {

	public static void main(String[] args) {
		
		Animal animal = new Cachorro("Tot�", 1, "Cachorro");
	
		// Imprimir 
		System.out.println("Ola, seu animal � " + animal.getEspecie() + ", o nome do " + 
		                   "animal � " + animal.getNome() + " e ele tem " + animal.getIdade() + " anos.");
		// Se e nao
		if (animal.ehAdulto()) {
			System.out.println("Animal Adulto");
		} else {
			System.out.println("Animal n�o � Adulto");
		}
		
		System.out.println("==============");
		System.out.println("Barrulho do Cachorro");
		animal.emitirBarrulho();
		
		animal.morrer();
		System.out.println("***************");
		System.out.println("==============");
		System.out.println("Barrulho do Cachorro");
		animal.emitirBarrulho();
		
		
		//Zoologico e lista
		Zoologico zoo = new Zoologico();
		zoo.setNome("Zoo TreinaWeb");
		
		//adicionar animais
		zoo.adcionarAnimal(animal);
		Animal animal2 = new Gato ("Z�", 3, "Gato");
		zoo.adcionarAnimal(animal2);		
		//imprimir animais no zoologico
			System.out.println("Animais do Zoologico " + zoo.getNome());
		zoo.listarAnimais();
		
		//remover animais
		zoo.removerAnimal(0);
			System.out.println("Depois da remocao");
		zoo.listarAnimais();
		
		
		Veterinario vet = new Veterinario();
		vet.setNome("Veterinario TreinaWeb");
		vet.atenderAnimal(animal);
		vet.listarAniamisAtendidos();
	}
	
	
}
